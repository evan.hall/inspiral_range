#!/usr/bin/env python

import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    long_description = f.read()

setup(
    name = 'inspiral_range',
    version = '0.1',
    description = long_description.splitlines()[0],
    long_description = long_description,
    author = ', '.join([
        'Jolien Creighton <jolien.creighton@ligo.org>',
        'Jameson Rollins <jameson.rollins@ligo.org>',
        ]),
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/jameson.rollins/inspiral_range',
    license = 'GNU GPL v3+',

    packages = ['inspiral_range'],
    package_data = {'inspiral_range': ['ang_avg.pkl']},

    # install_requires = [
    #     'lal',
    #     'lalsimulation',
    #     'scipy',
    #     ],

    entry_points={
        'console_scripts': [
            'inspiral-range = inspiral_range.__main__:main',
        ],
    },
)

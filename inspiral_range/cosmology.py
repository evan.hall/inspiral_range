import lal

# set cosmological parameters
# From Planck2015, Table IV
OMEGA = lal.CreateCosmologicalParametersAndRate().omega
lal.SetCosmologicalParametersDefaultValue(OMEGA)
OMEGA.h = 0.679
OMEGA.om = 0.3065
OMEGA.ol = 0.6935
OMEGA.ok = 1.0 - OMEGA.om - OMEGA.ol
OMEGA.w0 = -1.0
OMEGA.w1 = 0.0
OMEGA.w2 = 0.0

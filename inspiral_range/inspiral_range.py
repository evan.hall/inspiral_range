from __future__ import division
import logging
import collections
import numpy as np
import scipy
import scipy.constants
import scipy.optimize

import lal

from . import util
from . import waveform
from .cosmology import OMEGA
from .ang_avg import ang_avg

##################################################

CANONICAL_SNR = 8.0

##################################################

def int73(freq, psd):
    """Return "int73" full integral value and integrand array

    The integral is over the frequency component of the closed form
    inspiral SNR calculation, e.g.:

      \int_fmin^fmax df f^(-7/3) / psd

    @returns integral value as float, and np.array of integrand

    """
    assert len(freq) == len(psd)
    f73 = freq ** (-7/3)
    integrand73 = f73 / psd
    int73 = np.trapz(integrand73, freq)
    return int73, integrand73


def sensemon_range(freq, psd, m1=1.4, m2=1.4, horizon=False, **kwargs):
    """Detector inspiral range from closed form expression

    Masses `m1` and `m2` should be specified in solar masses (default:
    m1=m2=1.4).  If the `horizon` keyword is specified the "horizon"
    range will be returned, which differs from the angle-averaged
    range by ~2.26.

    @returns distance in Mpc as a float

    """
    assert len(freq) == len(psd)
    m1 *= lal.MSUN_SI
    m2 *= lal.MSUN_SI
    Mtot = m1 + m2
    mu = m1*m2/Mtot
    Mchirp = mu**(3/5) * Mtot**(2/5)
    # Mchirp = (m1*m2)**(3/5) / (m1 + m2)**(1/5)
    if horizon:
        theta = 4
    else:
        theta = 1.77
    C = 5 * Mchirp**(5/3) * theta**2 \
        / (96 * np.pi**(4/3) * CANONICAL_SNR**2) \
        * (scipy.constants.G**(5/3) / scipy.constants.c**3)
    return np.sqrt(C * int73(freq, psd)[0]) / 1e6 /lal.PC_SI

def sensemon_horizon(freq, psd, **kwargs):
    """Detector inspiral range horizon from closed form expression

    See sensemon_range() function.

    @returns horizon distance in Mpc as a float

    """
    return sensemon_range(freq, psd, horizon=True, **kwargs)

##################################################

def SNR(freq, psd, h):
    """SNR for given PSD and waveform

    @returns SNR as a float

    """
    rho = np.sqrt(np.trapz(4*(h**2/psd), freq))
    return rho


def SNR_z(freq, psd, z, omega=OMEGA, **params):
    """SNR of waveform with specified parameters at redshift z

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns SNR as a float

    """
    hplus, hcross = waveform.gen_waveform(freq, z=z, omega=omega, **params)
    # FIXME: should this be the average of hplus and hcross??
    rho = SNR(freq, psd, hplus)
    return rho


def horizon_redshift(freq, psd, omega=OMEGA, **params):
    """Detector horizon redshift

    For the given detector noise PSD and waveform parameters return
    the redshift at which the detection SNR would equal to the
    CANONICAL_SNR.

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns redshift as a float

    """
    assert len(freq) == len(psd)

    # we will performa a Brent method optimization to find the root of
    # the following function, thereby returning the z at which
    # SNR = CANONICAL_SNR:
    def opt_SNR_z(z):
        return SNR_z(freq, psd, z, omega, **params) - CANONICAL_SNR

    zmin = 1e-8 # must be less than horizon
    zmax = 10.0 # must be greater than horizon
    logging.debug('opt_SNR_z(zmin): {}'.format(opt_SNR_z(zmin)))
    logging.debug('opt_SNR_z(zmax): {}'.format(opt_SNR_z(zmax)))

    # A ValueError is returned if the ranges do not cover zero.  This
    # is probably because the zmax is not large enough, so bump the
    # max and try again.
    # FIXME: better checking of this (pre-check?)
    try:
        z_hor = scipy.optimize.brentq(opt_SNR_z, zmin, zmax)
    except ValueError:
        zmax = 100.0
        logging.debug("increasing zmax => {}...".format(zmax))
        logging.debug('opt_SNR_z(zmax): {}'.format(opt_SNR_z(zmax)))
        z_hor = scipy.optimize.brentq(opt_SNR_z, zmin, zmax)

    return z_hor


def horizon(freq, psd, omega=OMEGA, **params):
    """Detector horizon distance in Mpc

    See horizon_redshift().

    @returns distance in Mpc as a float

    """
    zhor = horizon_redshift(freq, psd, omega=omega, **params)
    return lal.LuminosityDistance(omega, zhor)


def volume(freq, psd, z_hor=None, omega=OMEGA, **params):
    """Detector redshift-corrected comoving volume

    For the given detector noise PSD and waveform parameters return
    the redshift-corrected, comoving volume in Mpc^3 within which all
    sources would have SNR greater than the CANONICAL_SNR.

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns distance in Mpc as a float

    """
    assert len(freq) == len(psd)

    # This volume is calculated based on the formalism described in
    # Belczynski et. al 2014:
    #
    #   https://dx.doi.org/10.1088/0004-637x/789/2/120
    #
    # The comoving sensitive volume is given by:
    #
    #   Vcbar = \Int_0^\inf dVc/dz 1/(1+z) f(z) dz
    #
    # where (dVc/dz 1/(1_z)) is the redshit-corrected "comoving
    # volumed density" and f(z) is the "detectability fraction" given
    # by a marginalization over the various orientation angles.
    #
    # We can cut off the integration at the horizon distance, z_hor,
    # since the assumption is that the SNR is below detectability
    # beyond.

    if not z_hor:
        z_hor = horizon_redshift(freq, psd, omega=omega, **params)

    # create a Gauss-Legendre quadrature for the integration:
    # https://en.wikipedia.org/wiki/Gaussian_quadrature#Gauss.E2.80.93Legendre_quadrature
    # x are the roots and w are the weights
    x, w = scipy.special.orthogonal.p_roots(20)
    # older versions of p_roots return complex roots
    x = np.real(x)
    # account for the fact that the interval is [0,z_hor], not [-1,1]
    z = 0.5 * z_hor * (x + 1.0)

    # detectability fraction
    snrs = np.array([SNR_z(freq, psd, zz, omega=omega, **params) for zz in z])
    f = np.array([ang_avg(snr / CANONICAL_SNR) for snr in snrs])
    # logging.debug('f = {}'.format(f))

    # comoving volume density, e.g. dVc/dz 1/(1+z)
    dVdz1pz = np.array([lal.UniformComovingVolumeDensity(zz, omega) for zz in z])

    # compute sensitivity volume in Mpc^3
    V = 0.5 * z_hor * sum(w * dVdz1pz * f)
    # # equivalent cosmology-corrected "sensemon" range
    # V0 = 0.5 * z_hor * sum(w * dVdz1pz)

    return V


def range(freq, psd, z_hor=None, omega=OMEGA, **params):
    """Detector redshift-corrected comoving range in Mpc

    For the given detector noise PSD and waveform parameters return
    the redshift-corrected, comoving distance in Mpc at which the
    detection SNR would equal to the CANONICAL_SNR, i.e. the radius of
    the Euclidean sphere given by volume().

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns distance in Mpc as a float

    """
    V = volume(freq, psd, z_hor=z_hor, omega=omega, **params)
    return util.v2r(V)


def response_frac_redshift(freq, psd, frac, omega=OMEGA, **params):
    """Detector response distance in Mpc

    For the given detector noise PSD and waveform parameters return
    the distance in Mpc at which the specified fraction of sources
    would be detected (SNR >= CANONICAL_SNR) if they were all placed
    at that distance.  Assumes a uniform distribution of sources.

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns distance in Mpc as a float

    """
    assert len(freq) == len(psd)

    # will perform Brent method optimization to find the root of this
    # function:
    def opt_f_z(z):
        snr = SNR_z(freq, psd, z, omega, **params)
        f = ang_avg(snr / CANONICAL_SNR)
        return f - frac

    zmin = 1e-8
    zmax = 10.0
    # logging.debug('opt_f_z(zmin): {}'.format(opt_f_z(zmin)))
    # logging.debug('opt_f_z(zmax): {}'.format(opt_f_z(zmax)))

    try:
        z,r = scipy.optimize.brentq(opt_f_z, zmin, zmax, full_output=True)
    except ValueError:
        zmax = 100.0
        logging.debug("increasing zmax=>{}...".format(zmax))
        # logging.debug('opt_f_z(zmax): {}'.format(opt_f_z(zmax)))
        z,r = scipy.optimize.brentq(opt_f_z, zmin, zmax, full_output=True)

    return z


def response_frac(freq, psd, frac, omega=OMEGA, **params):
    """Detector response distance in Mpc

    See reach_frac_redshift().

    @returns distance in Mpc as a float

    """
    z = response_frac_redshift(freq, psd, frac, omega=omega, **params)
    return lal.LuminosityDistance(omega, z)


def reach_frac_redshift(freq, psd, frac, omega=OMEGA, cvol=None, **params):
    """Detector detectability fraction reach redshift

    For the given detector noise PSD and waveform parameters return
    the distance at which the specified fraction of sources should be
    detected.  Assumes a uniform distribution of sources.

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns redshift as a float

    """
    assert len(freq) == len(psd)

    if not cvol:
        cvol = volume(freq, psd, omega=omega, **params)

    # will perform Brent method optimization to find the root of this
    # function:
    def opt_V_z(z):
        V = volume(freq, psd, z_hor=z, omega=omega, **params)
        return frac - V/cvol

    zmin = 1e-8
    zmax = 1.0
    logging.debug('opt_V_z(zmin): {}'.format(opt_V_z(zmin)))
    logging.debug('opt_V_z(zmax): {}'.format(opt_V_z(zmax)))

    try:
        z,r = scipy.optimize.brentq(opt_V_z, zmin, zmax, full_output=True)
    except ValueError:
        zmax = 100.0
        logging.debug("increasing zmax=>{}...".format(zmax))
        logging.debug('opt_V_z(zmax): {}'.format(opt_V_z(zmax)))
        z,r = scipy.optimize.brentq(opt_V_z, zmin, zmax, full_output=True)

    return z


def reach_frac(freq, psd, frac, omega=OMEGA, cvol=None, **params):
    """Detector detectability fraction reach in Mpc

    See reach_frac_redshift().

    @returns distance in Mpc as a float

    """
    z = reach_frac_redshift(freq, psd, frac, omega=omega, cvol=cvol, **params)
    return lal.LuminosityDistance(omega, z)


def cosmological_ranges(freq, psd, omega=OMEGA, **params):
    """Calculate various cosmology-corrected detector distance measures

    The following range values are calculated:

      horizon
      range
      response_50
      response_10
      reach_50
      reach_90

    See individual function help for more information.

    This method is faster than running all individual calculation
    methods separately, as various intermediate calculated values are
    used in the subsequent calculations to speed things up.

    Remaining keyword arguments are interpreted as waveform generation
    parameters (see waveform.gen_waveform()).

    @returns dictionary of range values as (value, 'unit') tuples

    """
    assert len(freq) == len(psd)

    z_hor = horizon_redshift(freq, psd, omega=omega, **params)
    logging.info('z_hor = {}'.format(z_hor))
    hor = lal.LuminosityDistance(omega, z_hor)

    cvol = volume(freq, psd, z_hor=z_hor, omega=omega, **params)
    logging.info('cvol = {}'.format(cvol))
    crange = util.v2r(cvol)

    response_50 = response_frac(freq, psd, 0.5, omega=omega, **params)
    logging.info('response_50 = {}'.format(response_50))
    response_10 = response_frac(freq, psd, 0.1, omega=omega, **params)
    logging.info('response_10 = {}'.format(response_10))

    reach_50 = reach_frac(freq, psd, 0.5, omega=omega, cvol=cvol, **params)
    logging.info('reach_50 = {}'.format(reach_50))
    reach_90 = reach_frac(freq, psd, 0.9, omega=omega, cvol=cvol, **params)
    logging.info('reach_90 = {}'.format(reach_90))

    return collections.OrderedDict([
        ('horizon', (hor, 'Mpc')),
        ('range',   (crange, 'Mpc')),
        ('response_50', (response_50, 'Mpc')),
        ('response_10', (response_10, 'Mpc')),
        ('reach_50', (reach_50, 'Mpc')),
        ('reach_90', (reach_90, 'Mpc')),
        ])

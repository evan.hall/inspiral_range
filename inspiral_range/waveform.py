from __future__ import division
import logging
from collections import OrderedDict
import numpy as np
import scipy

import lal
import lalsimulation

from .cosmology import OMEGA

##################################################

# default inspiral waveform parameters
# face-on 1.4/1.4 Msolar inspiral at 100 Mpc distance
DEFAULT_PARAMS = OrderedDict([
    ('approximant', None),
    ('distance', 100e6),
    ('m1', 1.4),
    ('m2', 1.4),
    ('S1x', 0.0),
    ('S1y', 0.0),
    ('S1z', 0.0),
    ('S2x', 0.0),
    ('S2y', 0.0),
    ('S2z', 0.0),
    ('inclination', 0.0),
    ('f_ref', 0.0),
    ('phiRef', 0.0),
    ('longAscNodes', 0.0),
    ('eccentricity', 0.0),
    ('meanPerAno', 0.0),
    ('deltaF', None),
    ('f_min', None),
    ('f_max', None),
    ('LALpars', None),
    ])

DEFAULT_APPROXIMANT_BNS = 'TaylorF2'
DEFAULT_APPROXIMANT_BBH = 'IMRPhenomD'

##################################################

def _get_waveform_params(**kwargs):
    params = OrderedDict(DEFAULT_PARAMS)
    params.update(**kwargs)
    # use waveform approximant appropriate to type
    if not params['approximant']:
        if params['m1'] >= 5 and params['m2'] >= 5:
            params['approximant'] = DEFAULT_APPROXIMANT_BBH
        else:
            params['approximant'] = DEFAULT_APPROXIMANT_BNS
    return params


def gen_waveform(freq, z=0, omega=OMEGA, **params):
    """Generate frequency-domain inspiral waveform

    `freq` should be an array of frequency points at which the
    waveform should be interpolated.  Returns a tuple of
    (h_tilde^plus, h_tilde^cross) real-valued (amplitude only) arrays.

    The waveform is generated with
    lalsimulation.SimInspiralChooseFDWaveform().  Keyword arguments
    are used to update the default waveform parameters (1.4/1.4
    Msolar, optimally-oriented, 100 Mpc, (see DEFAULT_PARAMS macro)).
    The mass parameters ('m1' and 'm2') should be specified in solar
    masses and the 'distance' parameter should be specified in
    parsecs**.  Waveform approximants may be given as string names
    (see `lalsimulation` documentation for more info).  If the
    approximant is not specified explicitly, DEFAULT_APPROXIMANT_BNS
    waveform will be used if either mass is less than 5 Msolar and
    DEFAULT_APPROXIMANT_BBH waveform will be used otherwise.

    If a redshift `z` is specified (with optional `omega`), it's
    equivalent distance will be used (ignoring any `distance`
    parameter provided) and the masses will be redshift-corrected
    appropriately.  Otherwise no mass redshift correction will be
    applied.

    For example, to generate a 20/20 Msolar BBH waveform:

    >>> hp,hc = waveform.gen_waveform(freq, 'm1'=20, 'm2'=20)

    **NOTE: The requirement that masses are specified in solar masses
    and distances are specified in parsecs is different than that of
    the underlying lalsimulation method which expects mass and
    distance parameters to be in SI units.

    """
    iparams = _get_waveform_params(**params)

    # if redshift specified use that as distance and correct
    # appropriately, ignoring any distance specified in params.
    if z != 0:
        iparams['distance'] = lal.LuminosityDistance(omega, z) * 1e6
        iparams['m1'] *= 1.0 + z
        iparams['m2'] *= 1.0 + z

    # convert to SI units
    iparams['distance'] *= lal.PC_SI
    iparams['m1'] *= lal.MSUN_SI
    iparams['m2'] *= lal.MSUN_SI
    iparams['approximant'] = lalsimulation.SimInspiralGetApproximantFromString(iparams['approximant'])

    iparams['deltaF'] = freq[1] - freq[0]
    iparams['f_min'] = freq[0]
    # FIXME: the max frequency in the generated waveform is not always
    # greater than f_max, so as a workaround we generate over the full
    # band.  Probably room for speedup here
    # iparams['f_max'] = freq[-1]
    iparams['f_max'] = 10000

    # logging.debug('waveform params = {}'.format(iparams))

    # generate waveform
    h = lalsimulation.SimInspiralChooseFDWaveform(**iparams)

    freq_h = h[0].f0 + np.arange(len(h[0].data.data)) * h[0].deltaF

    def interph(h):
        "interpolate amplitude of h array"
        # FIXME: this only interpolates/returns the amplitude, and not
        # the full complex data (throws out phase), because this was
        # not working:
        # hir = scipy.interpolate.interp1d(freq_h, np.real(h.data.data))(freq)
        # hii = scipy.interpolate.interp1d(freq_h, np.imag(h.data.data))(freq)
        # return hir + 1j * hii
        return scipy.interpolate.interp1d(freq_h, np.absolute(h.data.data))(freq)

    hi = map(interph, h)

    return hi
